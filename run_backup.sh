#!/bin/bash

# Make an LDAP backup
PATH=/opt/someApp/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
NOW=$(date +%y-%m-%d_%Hh%M)
LDAPBK=ldap_backup_$NOW.ldif
BACKUPDIR=~/backup
slapcat -v -b "dc=wethinkcode,dc=co,dc=za" -l $BACKUPDIR/$LDAPBK
md5sum $BACKUPDIR/$LDAPBK > $BACKUPDIR/$LDAPBK.MD5
gzip -9 $BACKUPDIR/$LDAPBK

# Delete old backup files
find /root/backup/ -type f -name '*ldif*' -mtime +6 -exec rm {} \;

# Push to offsite repository
git -C ~/backup add .
git -C ~/backup commit -m "Daily backup $NOW"
git -C ~/backup push origin master
